//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Cargadores
{
    using System;
    using System.Collections.Generic;
    
    public partial class RolxUsuario
    {
        public int idRolxUser { get; set; }
        public int idUsuario { get; set; }
        public int idRol { get; set; }
    
        public virtual Roles Roles { get; set; }
        public virtual Usuarios Usuarios { get; set; }
    }
}
