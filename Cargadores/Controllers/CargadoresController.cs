﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Cargadores;

namespace Cargadores.Controllers
{
    public class CargadoresController : Controller
    {
        private CargadoresEntities db = new CargadoresEntities();

        // GET: Cargadores
        public ActionResult Index()
        {
            var cargadores = db.Cargadores.Include(c => c.EstadoCargadores).Include(c => c.TipoCargadores).Include(c => c.Usuarios).Include(c => c.Prestamos);
            return View(cargadores.ToList());
        }

        // GET: Cargadores/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cargadores cargadores = db.Cargadores.Find(id);
            if (cargadores == null)
            {
                return HttpNotFound();
            }
            return View(cargadores);
        }

        // GET: Cargadores/Create
        public ActionResult Create()
        {
            ViewBag.IdEstado = new SelectList(db.EstadoCargadores, "IdEstado", "Estado");
            ViewBag.IdTipoCargador = new SelectList(db.TipoCargadores, "IdTipoCargador", "TipoCargador");
            ViewBag.IdUsuario = new SelectList(db.Usuarios, "IdUsuario", "Nombre");
            ViewBag.IdCargador = new SelectList(db.Prestamos, "IdCargador", "IdCargador");
            return View();
        }

        // POST: Cargadores/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdCargador,IdTipoCargador,IdEstado,IdUsuario")] Cargadores cargadores)
        {
            if (ModelState.IsValid)
            {
                db.Cargadores.Add(cargadores);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdEstado = new SelectList(db.EstadoCargadores, "IdEstado", "Estado", cargadores.IdEstado);
            ViewBag.IdTipoCargador = new SelectList(db.TipoCargadores, "IdTipoCargador", "TipoCargador", cargadores.IdTipoCargador);
            ViewBag.IdUsuario = new SelectList(db.Usuarios, "IdUsuario", "Nombre", cargadores.IdUsuario);
            ViewBag.IdCargador = new SelectList(db.Prestamos, "IdCargador", "IdCargador", cargadores.IdCargador);
            return View(cargadores);
        }

        // GET: Cargadores/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cargadores cargadores = db.Cargadores.Find(id);
            if (cargadores == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdEstado = new SelectList(db.EstadoCargadores, "IdEstado", "Estado", cargadores.IdEstado);
            ViewBag.IdTipoCargador = new SelectList(db.TipoCargadores, "IdTipoCargador", "TipoCargador", cargadores.IdTipoCargador);
            ViewBag.IdUsuario = new SelectList(db.Usuarios, "IdUsuario", "Nombre", cargadores.IdUsuario);
            ViewBag.IdCargador = new SelectList(db.Prestamos, "IdCargador", "IdCargador", cargadores.IdCargador);
            return View(cargadores);
        }

        // POST: Cargadores/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdCargador,IdTipoCargador,IdEstado,IdUsuario")] Cargadores cargadores)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cargadores).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdEstado = new SelectList(db.EstadoCargadores, "IdEstado", "Estado", cargadores.IdEstado);
            ViewBag.IdTipoCargador = new SelectList(db.TipoCargadores, "IdTipoCargador", "TipoCargador", cargadores.IdTipoCargador);
            ViewBag.IdUsuario = new SelectList(db.Usuarios, "IdUsuario", "Nombre", cargadores.IdUsuario);
            ViewBag.IdCargador = new SelectList(db.Prestamos, "IdCargador", "IdCargador", cargadores.IdCargador);
            return View(cargadores);
        }

        // GET: Cargadores/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cargadores cargadores = db.Cargadores.Find(id);
            if (cargadores == null)
            {
                return HttpNotFound();
            }
            return View(cargadores);
        }

        // POST: Cargadores/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Cargadores cargadores = db.Cargadores.Find(id);
            db.Cargadores.Remove(cargadores);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
