﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Cargadores;

namespace Cargadores.Controllers
{
    public class Cargadores1Controller : ApiController
    {
        private CargadoresEntities db = new CargadoresEntities();

        // GET: api/Cargadores1
        public IQueryable<Cargadores> GetCargadores()
        {
            return db.Cargadores;
        }

        // GET: api/Cargadores1/5
        [ResponseType(typeof(Cargadores))]
        public IHttpActionResult GetCargadores(int id)
        {
            Cargadores cargadores = db.Cargadores.Find(id);
            if (cargadores == null)
            {
                return NotFound();
            }

            return Ok(cargadores);
        }

        // PUT: api/Cargadores1/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCargadores(int id, Cargadores cargadores)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != cargadores.IdCargador)
            {
                return BadRequest();
            }

            db.Entry(cargadores).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CargadoresExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Cargadores1
        [ResponseType(typeof(Cargadores))]
        public IHttpActionResult PostCargadores(Cargadores cargadores)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Cargadores.Add(cargadores);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (CargadoresExists(cargadores.IdCargador))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = cargadores.IdCargador }, cargadores);
        }

        // DELETE: api/Cargadores1/5
        [ResponseType(typeof(Cargadores))]
        public IHttpActionResult DeleteCargadores(int id)
        {
            Cargadores cargadores = db.Cargadores.Find(id);
            if (cargadores == null)
            {
                return NotFound();
            }

            db.Cargadores.Remove(cargadores);
            db.SaveChanges();

            return Ok(cargadores);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CargadoresExists(int id)
        {
            return db.Cargadores.Count(e => e.IdCargador == id) > 0;
        }
    }
}