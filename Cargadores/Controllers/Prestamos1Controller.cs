﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Cargadores;

namespace Cargadores.Controllers
{
    public class Prestamos1Controller : ApiController
    {
        private CargadoresEntities db = new CargadoresEntities();

        // GET: api/Prestamos1
        public IQueryable<Prestamos> GetPrestamos()
        {
            return db.Prestamos;
        }

        // GET: api/Prestamos1/5
        [ResponseType(typeof(Prestamos))]
        public IHttpActionResult GetPrestamos(int id)
        {
            Prestamos prestamos = db.Prestamos.Find(id);
            if (prestamos == null)
            {
                return NotFound();
            }

            return Ok(prestamos);
        }

        // PUT: api/Prestamos1/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPrestamos(int id, Prestamos prestamos)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != prestamos.IdCargador)
            {
                return BadRequest();
            }

            db.Entry(prestamos).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PrestamosExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Prestamos1
        [ResponseType(typeof(Prestamos))]
        public IHttpActionResult PostPrestamos(Prestamos prestamos)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Prestamos.Add(prestamos);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (PrestamosExists(prestamos.IdCargador))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = prestamos.IdCargador }, prestamos);
        }

        // DELETE: api/Prestamos1/5
        [ResponseType(typeof(Prestamos))]
        public IHttpActionResult DeletePrestamos(int id)
        {
            Prestamos prestamos = db.Prestamos.Find(id);
            if (prestamos == null)
            {
                return NotFound();
            }

            db.Prestamos.Remove(prestamos);
            db.SaveChanges();

            return Ok(prestamos);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PrestamosExists(int id)
        {
            return db.Prestamos.Count(e => e.IdCargador == id) > 0;
        }
    }
}