USE [master]
GO
/****** Object:  Database [Cargadores]    Script Date: 21/05/2019 22:48:08 ******/
CREATE DATABASE [Cargadores]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Cargadores', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.DANIELFGSQL\MSSQL\DATA\Cargadores.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Cargadores_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.DANIELFGSQL\MSSQL\DATA\Cargadores_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [Cargadores] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Cargadores].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Cargadores] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Cargadores] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Cargadores] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Cargadores] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Cargadores] SET ARITHABORT OFF 
GO
ALTER DATABASE [Cargadores] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Cargadores] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Cargadores] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Cargadores] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Cargadores] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Cargadores] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Cargadores] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Cargadores] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Cargadores] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Cargadores] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Cargadores] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Cargadores] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Cargadores] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Cargadores] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Cargadores] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Cargadores] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Cargadores] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Cargadores] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Cargadores] SET  MULTI_USER 
GO
ALTER DATABASE [Cargadores] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Cargadores] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Cargadores] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Cargadores] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Cargadores] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'Cargadores', N'ON'
GO
ALTER DATABASE [Cargadores] SET QUERY_STORE = OFF
GO
USE [Cargadores]
GO
/****** Object:  Table [dbo].[Cargadores]    Script Date: 21/05/2019 22:48:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cargadores](
	[IdCargador] [int] IDENTITY(1,1) NOT NULL,
	[IdTipoCargador] [int] NOT NULL,
	[IdEstado] [int] NOT NULL,
	[IdUsuario] [int] NOT NULL,
 CONSTRAINT [PK_Cargadores] PRIMARY KEY CLUSTERED 
(
	[IdCargador] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EstadoCargadores]    Script Date: 21/05/2019 22:48:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EstadoCargadores](
	[IdEstado] [int] IDENTITY(1,1) NOT NULL,
	[Estado] [varchar](20) NOT NULL,
 CONSTRAINT [PK_EstadoCargadores] PRIMARY KEY CLUSTERED 
(
	[IdEstado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Prestamos]    Script Date: 21/05/2019 22:48:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Prestamos](
	[IdUsuarioDueño] [int] NOT NULL,
	[IdCargador] [int] NOT NULL,
	[IdUsuarioPrestamo] [int] NOT NULL,
	[Fecha] [date] NOT NULL,
 CONSTRAINT [PK_Prestamos] PRIMARY KEY CLUSTERED 
(
	[IdCargador] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 21/05/2019 22:48:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[idRol] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED 
(
	[idRol] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RolxUsuario]    Script Date: 21/05/2019 22:48:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RolxUsuario](
	[idRolxUser] [int] NOT NULL,
	[idUsuario] [int] NOT NULL,
	[idRol] [int] NOT NULL,
 CONSTRAINT [PK_RolxUsuario] PRIMARY KEY CLUSTERED 
(
	[idRolxUser] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TipoCargadores]    Script Date: 21/05/2019 22:48:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoCargadores](
	[IdTipoCargador] [int] IDENTITY(1,1) NOT NULL,
	[TipoCargador] [varchar](30) NOT NULL,
 CONSTRAINT [PK_TipoCargadores] PRIMARY KEY CLUSTERED 
(
	[IdTipoCargador] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Usuarios]    Script Date: 21/05/2019 22:48:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuarios](
	[Nombre] [varchar](50) NOT NULL,
	[nUsuario] [varchar](50) NOT NULL,
	[Contraseña] [varchar](50) NOT NULL,
	[Correo] [varchar](50) NOT NULL,
	[IdUsuario] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_Usuarios] PRIMARY KEY CLUSTERED 
(
	[IdUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Cargadores]  WITH CHECK ADD  CONSTRAINT [FK_Cargadores_EstadoCargadores] FOREIGN KEY([IdEstado])
REFERENCES [dbo].[EstadoCargadores] ([IdEstado])
GO
ALTER TABLE [dbo].[Cargadores] CHECK CONSTRAINT [FK_Cargadores_EstadoCargadores]
GO
ALTER TABLE [dbo].[Cargadores]  WITH CHECK ADD  CONSTRAINT [FK_Cargadores_TipoCargadores] FOREIGN KEY([IdTipoCargador])
REFERENCES [dbo].[TipoCargadores] ([IdTipoCargador])
GO
ALTER TABLE [dbo].[Cargadores] CHECK CONSTRAINT [FK_Cargadores_TipoCargadores]
GO
ALTER TABLE [dbo].[Cargadores]  WITH CHECK ADD  CONSTRAINT [FK_Cargadores_Usuarios] FOREIGN KEY([IdUsuario])
REFERENCES [dbo].[Usuarios] ([IdUsuario])
GO
ALTER TABLE [dbo].[Cargadores] CHECK CONSTRAINT [FK_Cargadores_Usuarios]
GO
ALTER TABLE [dbo].[Prestamos]  WITH CHECK ADD  CONSTRAINT [FK_Prestamos_Cargadores] FOREIGN KEY([IdCargador])
REFERENCES [dbo].[Cargadores] ([IdCargador])
GO
ALTER TABLE [dbo].[Prestamos] CHECK CONSTRAINT [FK_Prestamos_Cargadores]
GO
ALTER TABLE [dbo].[Prestamos]  WITH CHECK ADD  CONSTRAINT [FK_Prestamos_Usuarios] FOREIGN KEY([IdUsuarioDueño])
REFERENCES [dbo].[Usuarios] ([IdUsuario])
GO
ALTER TABLE [dbo].[Prestamos] CHECK CONSTRAINT [FK_Prestamos_Usuarios]
GO
ALTER TABLE [dbo].[Prestamos]  WITH CHECK ADD  CONSTRAINT [FK_Prestamos_Usuarios1] FOREIGN KEY([IdUsuarioPrestamo])
REFERENCES [dbo].[Usuarios] ([IdUsuario])
GO
ALTER TABLE [dbo].[Prestamos] CHECK CONSTRAINT [FK_Prestamos_Usuarios1]
GO
ALTER TABLE [dbo].[RolxUsuario]  WITH CHECK ADD  CONSTRAINT [FK_RolxUsuario_Roles] FOREIGN KEY([idRol])
REFERENCES [dbo].[Roles] ([idRol])
GO
ALTER TABLE [dbo].[RolxUsuario] CHECK CONSTRAINT [FK_RolxUsuario_Roles]
GO
ALTER TABLE [dbo].[RolxUsuario]  WITH CHECK ADD  CONSTRAINT [FK_RolxUsuario_Usuarios] FOREIGN KEY([idUsuario])
REFERENCES [dbo].[Usuarios] ([IdUsuario])
GO
ALTER TABLE [dbo].[RolxUsuario] CHECK CONSTRAINT [FK_RolxUsuario_Usuarios]
GO
USE [master]
GO
ALTER DATABASE [Cargadores] SET  READ_WRITE 
GO
